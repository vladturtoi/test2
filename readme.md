# Install steps

#### 1) git clone https://vladturtoi@bitbucket.org/vladturtoi/test.git
#### 2) sudo chmod -R 777 storage/
#### 3) copy .env.example in .env and set up DB
#### 4) composer install
#### 5) php artisan key:generate and copy the string between [] to the APP_KEY in .env
#### 6) php artisan migrate --seed

# Info

### Routes

#### /auth - for login
#### /update - for update

### There are 3 dummy users

#### testuser / testpass
#### testuser2 / testpass2
#### testuser3 / testpass3

